package com.product.demo.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ResponseDto {

	private int statusCode;
	private String message;
}
