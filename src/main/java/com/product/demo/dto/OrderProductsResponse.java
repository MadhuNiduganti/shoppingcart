package com.product.demo.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class OrderProductsResponse {

	private Integer productId;
	private String productName;
	private Integer quantity;
	private Double price;
}
