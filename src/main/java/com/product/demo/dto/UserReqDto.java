package com.product.demo.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserReqDto {

	private String name;
	private String phone;
	private String email;
	private String userType;
	private String password;
	private String confirmPassword;
}
