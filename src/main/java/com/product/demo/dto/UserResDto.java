package com.product.demo.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserResDto {

	private int statusCode;
	private String message;
	private String userName;
}
