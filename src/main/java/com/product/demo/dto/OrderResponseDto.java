package com.product.demo.dto;

import java.time.LocalDateTime;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class OrderResponseDto {

	private Integer orderId; 
	private LocalDateTime orderDate;
	List<OrderProductsResponse> OrderProductsResponses;
 
}
