package com.product.demo.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserDto {

	private String email;
	private String password;
}
