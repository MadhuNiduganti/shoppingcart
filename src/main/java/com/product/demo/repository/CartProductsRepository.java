package com.product.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.product.demo.entity.CartProducts;
@Repository
public interface CartProductsRepository extends JpaRepository<CartProducts, Integer> {
	@Transactional
	  @Modifying
	@Query(value="delete from CartProducts cp where cp.cartId=?1")
	public int deleteByCartId( Integer cartId);

	Optional<List<CartProducts>> findByCartId(Integer cartId);

}
