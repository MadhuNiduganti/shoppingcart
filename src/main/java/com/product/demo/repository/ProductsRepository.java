package com.product.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.product.demo.entity.Products;
@Repository
public interface ProductsRepository extends JpaRepository<Products, Integer>  {
	
	@Query("select p from Products p where p.productName like %?1%")
	List<Products>findUsingLike(String productName);
	
	@Query("select p from Products p where p.productId=?1")
	Optional<Products> findOne(Integer integer);


}
