package com.product.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.product.demo.entity.Users;
@Repository
public interface UserRepository extends JpaRepository<Users, Integer> {

//	public Optional<Users> findById(Long userId);
	
	public Users findByEmailAndPhone(String email, String phone);
	
	Users findByEmailAndPassword(String email, String password);
}
