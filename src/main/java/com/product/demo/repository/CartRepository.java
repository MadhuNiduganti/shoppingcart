package com.product.demo.repository;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.product.demo.entity.Cart;
@Repository
public interface CartRepository extends JpaRepository<Cart,Integer>{
//	 @Query("select u from Cart u where u.userId=?1 and u.status=?2")
	    public  Cart findByUserIdAndStatus(Integer userId,String cartStatus);
	    
	    @Transactional
	      @Modifying
	    @Query("delete from Cart c where c.userId=?1 and c.cartId=?2")
	    public int deleteByUserIdandCartId(Integer userId,Integer cartId);

	 
	
	Optional<Cart> findByCartIdAndStatus(Integer cartId,String status);

}
