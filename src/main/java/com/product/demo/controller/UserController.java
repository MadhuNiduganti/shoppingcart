package com.product.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.product.demo.dto.OrderResponseDto;
import com.product.demo.dto.ResponseDto;
import com.product.demo.dto.UserDto;
import com.product.demo.dto.UserReqDto;
import com.product.demo.dto.UserResDto;
import com.product.demo.exception.CredentialMissmatchException;
import com.product.demo.exception.UserExistException;
import com.product.demo.service.OrderService;
import com.product.demo.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	UserService userService;
	@Autowired
	OrderService orderService;
	
	@PostMapping("/userRegistration")
	public ResponseEntity<UserResDto> userRegistration(@RequestBody UserReqDto userReqDto)
			throws UserExistException, CredentialMissmatchException {
		UserResDto userResDto = userService.userRegistration(userReqDto);
		return new ResponseEntity<>(userResDto, HttpStatus.OK);
	}
	
	@PostMapping("login")
	public ResponseEntity<ResponseDto> userLogin(@RequestBody UserDto userDto) {
		ResponseDto responseDto = userService.userLogin(userDto);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}
	
	
	@GetMapping("{userId}/orders")
	public ResponseEntity<Optional<List<OrderResponseDto>>> getOrders(@PathVariable("userId") Integer userId, @RequestParam(name = "pageNumber")Integer pageNumber,@RequestParam(name = "pageSize")Integer pageSize) {
		return new ResponseEntity<>(orderService.getOrders(userId,pageNumber,pageSize), HttpStatus.OK);
	}

	
}
