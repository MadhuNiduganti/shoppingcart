package com.product.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.product.demo.dto.CartReqDto;
import com.product.demo.dto.CartResDto;
import com.product.demo.exception.CredentialMissmatchException;
import com.product.demo.exception.ProductNotFoundException;
import com.product.demo.exception.UserNotFoundException;
import com.product.demo.service.CartService;
@RestController
public class CartController {
	
	@Autowired
	CartService cartService;
	
	@PostMapping("/users/{userId}/carts")
	public ResponseEntity<CartResDto> addCart(@RequestBody CartReqDto cartReqDto,@PathVariable("userId") Integer userId) 
			throws UserNotFoundException, CredentialMissmatchException, ProductNotFoundException {
		CartResDto cartResDto = cartService.addCart(cartReqDto,userId);
		return new ResponseEntity<>(cartResDto, HttpStatus.OK);
	}
	
	@DeleteMapping("/users/{userId}/carts/{cartId}")
	public ResponseEntity<CartResDto> removeCart(@PathVariable("userId") Integer userId,@PathVariable("cartId") Integer cartId) 
			throws UserNotFoundException, CredentialMissmatchException, ProductNotFoundException {
		CartResDto cartResDto = cartService.removeCart(userId,cartId);
		return new ResponseEntity<>(cartResDto, HttpStatus.OK);
	}

}
