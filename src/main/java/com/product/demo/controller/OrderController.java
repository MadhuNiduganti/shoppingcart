package com.product.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.product.demo.dto.OrderDto;
import com.product.demo.dto.ResponseDto;
import com.product.demo.service.OrderService;


/**
 * 
 * @author allam.sairam
 *
 */
@RestController
@RequestMapping("/orders")
public class OrderController {

	@Autowired
	OrderService orderService;
	@PostMapping("")
	public ResponseEntity<Optional<ResponseDto>> placeOrder(@RequestBody OrderDto orderDto) {
		
		return ResponseEntity.ok().body(orderService.placeOrder(orderDto));
		
	}
}
