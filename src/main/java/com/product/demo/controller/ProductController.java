package com.product.demo.controller;

 
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.product.demo.dto.ProductDto;
import com.product.demo.exception.ProductNotFoundException;
import com.product.demo.service.ProductService;

@RestController
public class ProductController {
	
	Logger logger = LoggerFactory.getLogger(ProductController.class);
	@Autowired
	ProductService productService;
	
	 @GetMapping(value = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
	    public List<ProductDto> getProductsByQuery(@RequestParam("productName") String productName) throws ProductNotFoundException{
	        logger.info("Fetching product with query " + productName);
	        List<ProductDto> productList = productService.findByQuery(productName);
	         	      // logger.info("\n customer get id "+card.get().getCustomer().getId());
	       
	        return productList;
	    }
	    

}
