package com.product.demo.exception;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ErrorResponse {

	private String message;
	private Integer statusCode;

}
