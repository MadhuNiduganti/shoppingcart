package com.product.demo.exception;

public class UserNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -1747502351308293745L;

	public UserNotFoundException(String message) {
		super(message);
	}
}
