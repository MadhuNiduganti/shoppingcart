package com.product.demo.exception;

public class ProductNotFoundException extends Exception {

	private static final long serialVersionUID = -1747502351308293745L;

	public ProductNotFoundException(String message) {
		super(message);
	}
}
