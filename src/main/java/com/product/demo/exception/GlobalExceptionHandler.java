package com.product.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	  
	  @ExceptionHandler(UserException.class)
	  public ResponseEntity<ErrorResponse> ecomorseExceptionHandler(UserException ex,WebRequest request) {
		  ErrorResponse errorResponse = new ErrorResponse();
		  errorResponse.setMessage(ex.getMessage());
		  errorResponse.setStatusCode(602);
	  return new ResponseEntity<>(errorResponse,HttpStatus.OK);
	  
	  }
	  
	  @ExceptionHandler(CartException.class)
	  public ResponseEntity<ErrorResponse> ecomorseExceptionHandler(CartException ex,WebRequest request) {
		  ErrorResponse errorResponse = new ErrorResponse();
		  errorResponse.setMessage(ex.getMessage());
		  errorResponse.setStatusCode(601);
	  return new ResponseEntity<>(errorResponse,HttpStatus.OK);
	  
	  }
	 
	  @ExceptionHandler(OrderException.class)
	  public ResponseEntity<ErrorResponse> ecomorseExceptionHandler(OrderException ex,WebRequest request) {
		  ErrorResponse errorResponse = new ErrorResponse();
		  errorResponse.setMessage(ex.getMessage());
	  
	  return new ResponseEntity<>(errorResponse,HttpStatus.OK);
	  
	  }
	 

	  @ExceptionHandler(Exception.class)
		public ResponseEntity<ErrorResponse> globalExceptionHandler(Exception exception, WebRequest request) {
			 ErrorResponse errorResponse = new ErrorResponse();
			  errorResponse.setMessage(exception.getMessage());
			  errorResponse.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);

		}

}