package com.product.demo.exception;

public class CredentialMissmatchException extends Exception {
	
	private static final long serialVersionUID = -1747502351308293745L;

	public CredentialMissmatchException(String message) {
		super(message);
	}

}
