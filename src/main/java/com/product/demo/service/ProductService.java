package com.product.demo.service;

import java.util.List;

import com.product.demo.dto.ProductDto;
import com.product.demo.entity.Products;
import com.product.demo.exception.ProductNotFoundException;

public interface ProductService {
	
	List<ProductDto> findByQuery(String product) throws ProductNotFoundException;
	ProductDto entittTodto(Products products);
 
}
