package com.product.demo.service;

import com.product.demo.dto.ResponseDto;
import com.product.demo.dto.UserDto;
import com.product.demo.dto.UserReqDto;
import com.product.demo.dto.UserResDto;
import com.product.demo.exception.CredentialMissmatchException;
import com.product.demo.exception.UserExistException;

public interface UserService {

	public ResponseDto userLogin(UserDto userDto);

	public UserResDto userRegistration(UserReqDto userReqDto) throws UserExistException, CredentialMissmatchException;

}
