package com.product.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.product.demo.dto.ResponseDto;
import com.product.demo.dto.UserDto;
import com.product.demo.dto.UserReqDto;
import com.product.demo.dto.UserResDto;
import com.product.demo.entity.Users;
import com.product.demo.exception.CredentialMissmatchException;
import com.product.demo.exception.UserExistException;
import com.product.demo.repository.UserRepository;
import com.product.demo.service.UserService;
import com.product.demo.utility.ErrorConstant;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserRepository userRepository;

	@Override
	public UserResDto userRegistration(UserReqDto userReqDto) throws UserExistException, CredentialMissmatchException {

		UserResDto userResDto = new UserResDto();
		Users user;
		user = userRepository.findByEmailAndPhone(userReqDto.getEmail(), userReqDto.getPhone());

		if (user != null) {

			throw new UserExistException(ErrorConstant.USER_EXIST);
		} else {
			if (!(userReqDto.getPassword().equals(userReqDto.getConfirmPassword())))
				throw new CredentialMissmatchException(ErrorConstant.CREDENTIAL_MISSMATCH);
			
			user = new Users();
			user.setPassword(userReqDto.getPassword());
			user.setEmail(userReqDto.getEmail());
			user.setPhone(userReqDto.getPhone());
			user.setUserName(userReqDto.getName());
			userRepository.save(user);
			userResDto.setStatusCode(ErrorConstant.USER_REGISTERED_CODE);
			userResDto.setMessage(ErrorConstant.USER_REGISTERED);
			userResDto.setUserName(userReqDto.getEmail() + "is your username");

		}
		return userResDto;

	}
	
	@Override
	public ResponseDto userLogin(UserDto userDto) {
		ResponseDto responseDto = new ResponseDto();

		if (userDto.getEmail().isEmpty() || userDto.getPassword().isEmpty()) {
			responseDto.setStatusCode(ErrorConstant.INVALID_INPUT_CODE);
			responseDto.setMessage(ErrorConstant.INVALID_INPUT);

		} else {
			Users user = userRepository.findByEmailAndPassword(userDto.getEmail(), userDto.getPassword());

			if (user != null) {
				responseDto.setStatusCode(ErrorConstant.LOGIN_SUCCESS_CODE);
				responseDto.setMessage(ErrorConstant.LOGIN_SUCCESS);
			} else {
				responseDto.setStatusCode(ErrorConstant.LOGIN_FAIL_CODE);
				responseDto.setMessage(ErrorConstant.LOGIN_FAIL);

			}
		}
		return responseDto;
	}
}
