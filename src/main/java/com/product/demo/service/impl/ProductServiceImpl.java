package com.product.demo.service.impl;

import java.util.ArrayList;
import java.util.List;

//import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.product.demo.dto.ProductDto;
import com.product.demo.entity.Products;
import com.product.demo.exception.ProductNotFoundException;
import com.product.demo.repository.ProductsRepository;
import com.product.demo.service.ProductService;

@Service
@Transactional
public class ProductServiceImpl implements ProductService{
	
	@Autowired
	ProductsRepository objProductRepository;
	
	/*
	 * @Autowired private ModelMapper modelMapper;
	 */

	public List<ProductDto> findByQuery(String product) throws ProductNotFoundException{
		
		List<Products> prdList =objProductRepository.findUsingLike(product);
		
		if(prdList.isEmpty()) {
			System.out.println("\n\nProduct not found");
			throw new ProductNotFoundException("Product not found");
		}
		
		List<ProductDto> prddtoList = new ArrayList<>();
		for(Products products:prdList) {
			prddtoList.add(entittTodto(products));
		}
		
		    
		
		return prddtoList;
	}
	
	
	
	
	public   ProductDto entittTodto(Products products) {
		ProductDto dto = new ProductDto();
		
		dto.setProductId(products.getProductId());
		dto.setProductName(products.getProductName());
		dto.setPrice(products.getPrice());

		/*
		 * ModelMapper modelMapper = new ModelMapper(); // user here is a prepopulated
		 * User instance ProductDto prdoductDto = modelMapper.map(products,
		 * ProductDto.class);
		 */

	    return dto;
	    }
	
}