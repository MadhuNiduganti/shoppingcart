package com.product.demo.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.product.demo.dto.OrderDto;
import com.product.demo.dto.OrderProductsResponse;
import com.product.demo.dto.OrderResponseDto;
import com.product.demo.dto.ResponseDto;
import com.product.demo.entity.Cart;
import com.product.demo.entity.CartProducts;
import com.product.demo.entity.OrderDetails;
import com.product.demo.entity.Products;
import com.product.demo.exception.CartException;
import com.product.demo.exception.OrderException;
import com.product.demo.exception.UserNotFoundException;
import com.product.demo.repository.CartProductsRepository;
import com.product.demo.repository.CartRepository;
import com.product.demo.repository.OrderDetailsRepository;
import com.product.demo.repository.ProductsRepository;
import com.product.demo.repository.UserRepository;
import com.product.demo.service.OrderService;
import com.product.demo.utility.CartStatus;
/**
 * 
 * @author allam.sairam
 *
 */
@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	OrderDetailsRepository orderDetailsRepository;

	@Autowired
	CartRepository cartRepository;
	@Autowired
	CartProductsRepository cartProductsRepository;

	@Autowired
	ProductsRepository productsRepository;

	@Override
	public Optional<ResponseDto> placeOrder(OrderDto orderDto) {
		
		if(!userRepository.findById(orderDto.getUserId()).isPresent()) {
			throw new UserNotFoundException("invalid user");
		}
		Optional<Cart> cart = cartRepository.findById(orderDto.getCartId());

		if(!cart.isPresent()) {
			throw new CartException("no valid cart existed");
		}
		
		if(cart.get().getStatus().equalsIgnoreCase(CartStatus.ORDERED.name())) {
			throw new CartException("no valid cart existed");
		}
		
		//need to check the status also
		OrderDetails orderDetails=new OrderDetails();
		BeanUtils.copyProperties(orderDto, orderDetails);
		orderDetails.setOrderDate(LocalDateTime.now());
		orderDetailsRepository.save(orderDetails);
		
		cart.get().setStatus(CartStatus.ORDERED.name());
		cartRepository.save(cart.get());
		
		ResponseDto responseDto=new ResponseDto();
		responseDto.setMessage("successfully order placed");
		responseDto.setStatusCode(HttpStatus.CREATED.value());
		return Optional.of(responseDto);
	}

	
	@Override
	public Optional<List<OrderResponseDto>> getOrders(Integer userId,Integer pageNumber,Integer pageSize) {
		
		Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(Order.desc("orderDate")));

		if (!userRepository.findById(userId).isPresent()) {
			throw new OrderException("invalid user");
		}

		List<OrderResponseDto> orderResponseDtoList = new ArrayList<>();

		Optional<List<OrderDetails>> orderDetailsListOptional = orderDetailsRepository.findByUserId(userId,pageable);

		orderDetailsListOptional.ifPresent(orderDetailsList -> {
			orderDetailsList.stream().forEach(orderDetails -> {

				OrderResponseDto orderResponseDto = new OrderResponseDto();
				orderResponseDto.setOrderDate(orderDetails.getOrderDate());
				orderResponseDto.setOrderId(orderDetails.getOrderId());

				Optional<List<CartProducts>> cartOptional = cartProductsRepository
						.findByCartId(orderDetails.getCartId());
				List<OrderProductsResponse> orderProductsResponseList = new ArrayList<>();
				cartOptional.ifPresent(cartList -> {
					cartList.stream().forEach(cart -> {
						Optional<Products> productOptional = productsRepository.findById(cart.getProductId());

						productOptional.ifPresent(product -> {
							OrderProductsResponse orderProductsResponse = new OrderProductsResponse();
							orderProductsResponse.setPrice(cart.getPrice());
							orderProductsResponse.setProductId(product.getProductId());
							orderProductsResponse.setProductName(product.getProductName());
							orderProductsResponse.setQuantity(cart.getQuantity());
							orderProductsResponseList.add(orderProductsResponse);
						});
					});

				});
				orderResponseDto.setOrderProductsResponses(orderProductsResponseList);
				orderResponseDtoList.add(orderResponseDto);

			});

		}

		);
		return Optional.of(orderResponseDtoList);
	}

}
