package com.product.demo.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.product.demo.dto.CartReqDto;
import com.product.demo.dto.CartResDto;
import com.product.demo.entity.Cart;
import com.product.demo.entity.CartProducts;
import com.product.demo.entity.Products;
import com.product.demo.entity.Users;
import com.product.demo.exception.ProductNotFoundException;
import com.product.demo.exception.UserNotFoundException;
import com.product.demo.repository.CartProductsRepository;
import com.product.demo.repository.CartRepository;
import com.product.demo.repository.ProductsRepository;
import com.product.demo.repository.UserRepository;
import com.product.demo.service.CartService;
import com.product.demo.utility.CartStatus;

@Service
@Transactional
public class CartServiceImpl implements CartService {
	
	@Autowired
	UserRepository userRepository;
	@Autowired
	CartRepository cartRepository;
	@Autowired
	ProductsRepository productRepository;
	@Autowired
	CartProductsRepository cartproductsRepository;
	
	
	 CartProducts cartProducts;
	
	  
	 
	 @Override
	public CartResDto addCart(CartReqDto cartReqDto,Integer userId) throws UserNotFoundException,ProductNotFoundException{
		
		 Optional<Users> users=userRepository.findById(userId);
		 Optional<Products> products=productRepository.findOne(cartReqDto.getProductId());
		 
		 System.out.println(users.isPresent());
		 if(!users.isPresent()) {
			 throw new UserNotFoundException(userId +" not found");
		 }else if(products==null){
			 throw new ProductNotFoundException(cartReqDto.getProductId() +" not found");
		 }else {
			
			  Cart cart=cartRepository.findByUserIdAndStatus(userId,CartStatus.CREATED.name());
			 if(cart==null  ) {
				 Cart objCart =new Cart();
				 objCart.setUserId(userId);
				 objCart.setStatus(CartStatus.CREATED.name());
				 
				 cartRepository.save(objCart);
				 addCartProduct(products.get(),objCart,cartReqDto);
				 
			 }else {
				 System.out.println("product id "+products.get().getProductName());
				 System.out.println("user id "+cart.getUserId());
				 addCartProduct(products.get(),cart ,cartReqDto);
			 }
			 CartResDto resDto = new CartResDto();
			 resDto.setStatusCode(200);
			 resDto.setStatusMessage("Product added in your cart");
			 
			 return resDto;
		 }
		
		 
	}
	 
	 @Override
	public CartResDto removeCart(Integer userId,Integer cartId) {
		 
		 Cart objCart =new Cart();
		 objCart.setCartId(cartId);
		 objCart.setUserId(userId);
		 objCart.setStatus(CartStatus.CANCELED.name());
		 cartRepository.save(objCart);
				
		 
		 CartResDto resDto = new CartResDto();
		 resDto.setStatusCode(200);
		 resDto.setStatusMessage("Product removed in your cart");
		 return resDto;
		 
	 }
		
	
	public void addCartProduct(Products products,Cart cart,CartReqDto cartReqDto) {
		cartProducts= new CartProducts();
		cartProducts.setCartId(cart.getCartId());
		cartProducts.setProductId(products.getProductId());
		cartProducts.setPrice(products.getPrice());
		cartProducts.setQuantity(cartReqDto.getQuantity());
		cartproductsRepository.save(cartProducts);
	}



	
	 


}
