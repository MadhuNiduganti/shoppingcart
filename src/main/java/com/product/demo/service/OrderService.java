package com.product.demo.service;

import java.util.List;
import java.util.Optional;

import com.product.demo.dto.OrderDto;
import com.product.demo.dto.OrderResponseDto;
import com.product.demo.dto.ResponseDto;
/**
 * 
 * @author allam.sairam
 *
 */
public interface OrderService {

	Optional<ResponseDto> placeOrder(OrderDto orderDto);
	Optional<List<OrderResponseDto>> getOrders(Integer userId,Integer pageNumber,Integer pageSize);

}
