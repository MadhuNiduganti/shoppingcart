package com.product.demo.service;

import com.product.demo.dto.CartReqDto;
import com.product.demo.dto.CartResDto;
 
import com.product.demo.exception.ProductNotFoundException;
import com.product.demo.exception.UserNotFoundException;

public interface CartService {
	
	CartResDto addCart(CartReqDto cartReqDto,Integer userId) throws UserNotFoundException,ProductNotFoundException;
	
	CartResDto removeCart(Integer userId,Integer cartId);
	
	

}
