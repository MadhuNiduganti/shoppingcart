package com.product.demo.utility;

public class ErrorConstant {

	private ErrorConstant() {

	}
	
	public static final String PRODUCTS_ADDED_SUCC = "Products added successfully";
	public static final int PRODUCTS_ADDED_SUCC_CODE = 200;

	public static final String USER_EXIST = "already registered";
	public static final int USER_EXIST_CODE = 605;

	public static final String USER_REGISTERED = "registered successfully";
	public static final int USER_REGISTERED_CODE = 607;

	public static final String INVALID_INPUT = "Please provide the required  data";
	public static final int INVALID_INPUT_CODE = 602;

	public static final String LOGIN_SUCCESS = "logged in successfully";
	public static final int LOGIN_SUCCESS_CODE = 603;

	public static final String LOGIN_FAIL = "invalid username or password";
	public static final int LOGIN_FAIL_CODE = 604;

	public static final String CREDENTIAL_MISSMATCH = "password and confirm password must be same";
	public static final int CREDENTIAL_MISSMATCH_CODE = 606;

}
