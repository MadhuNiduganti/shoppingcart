package com.product.demo.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.product.demo.dto.ProductDto;
import com.product.demo.repository.ProductsRepository;
import com.product.demo.service.impl.ProductServiceImpl;

//@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest
public class ProductControllerTest {
	
	@InjectMocks
	ProductController productController;
	
	@Mock
	private ProductServiceImpl productService;
	
	@Mock
	private ProductsRepository productRepository;
	
	
	@Test
	public void searchProduct() throws Exception{
		
		List<ProductDto> prodList = new ArrayList<>();
		prodList.add(new ProductDto(1,"Samung Mobile k7",10000.00));
		prodList.add(new ProductDto(2,"honor bee mobile",8000.00));
		 
		when(productService.findByQuery("Mobile")).thenReturn(prodList);
		
		List<ProductDto> result = productController.getProductsByQuery("Mobile");
		
		
		assertEquals(prodList.get(1).getProductId(), result.get(1).getProductId());
		/*RequestBuilder request = MockMvcRequestBuilders.get("/products").queryParam("productName", "Mobile").accept(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(request).andExpect(status().isOk()).andExpect(content().json("[{\"productId\":1,\"productName\":\"Samung Mobile k7\",\"price\":10000.00},{\"productId\":2,\"productName\":\"honor bee mobile\",\"price\":8000.00}]")).andReturn();
		//MvcResult result = mockMvc.perform(request).andExpect(status().isOk()).andExpect(content().json("{\"id\":2,\"name\":\"samuel\",\"street\":\"Rose Street\",\"country\":\"USA\"}")).andReturn();

		//MvcResult result = mockMvc.perform(request).andReturn();
		//MvcResult result = mockMvc.perform(request).andReturn();
				//.andExpect(status().isOk()).andExpect(content().json("{\"id\":2,\"name\":\"samuel\",\"street\":\"Rose Street\",\"country\":\"USA\",\"account\":{\"accountNo\":100001,\"amount\":10800.0}}")).andReturn();
		System.out.println("Status  "+result.getResponse().getStatus());*/
		
		//assertEquals("7",result.getResponse().getContentAsString());
	}

	 

}
