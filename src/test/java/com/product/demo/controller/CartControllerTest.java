package com.product.demo.controller;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.product.demo.dto.CartReqDto;
import com.product.demo.dto.CartResDto;
import com.product.demo.exception.CredentialMissmatchException;
import com.product.demo.exception.ProductNotFoundException;
import com.product.demo.exception.UserExistException;
import com.product.demo.exception.UserNotFoundException;
import com.product.demo.service.CartService;

//@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest
public class CartControllerTest {

	@InjectMocks
	CartController cartController;
	
	@Mock
	CartService cartService;

	@Test
	public void addCart() throws UserExistException, CredentialMissmatchException, UserNotFoundException, ProductNotFoundException {
		CartReqDto cartReqDto = new CartReqDto();
		cartReqDto.setProductId(1);
		cartReqDto.setQuantity(3);
		
		

		CartResDto responseDto = new CartResDto();
		responseDto.setStatusCode(200);
		responseDto.setStatusMessage("Product added in your cart");

		Mockito.when(cartService.addCart(cartReqDto, 1)).thenReturn(responseDto);
		ResponseEntity<CartResDto> result = cartController.addCart(cartReqDto, 1);

		assertEquals(HttpStatus.OK, result.getStatusCode());

	}

	@Test
	public void removeCart() throws UserExistException, CredentialMissmatchException, UserNotFoundException, ProductNotFoundException {
		CartReqDto cartReqDto = new CartReqDto();
		cartReqDto.setProductId(1);
		cartReqDto.setQuantity(3);
		
		

		CartResDto responseDto = new CartResDto();
		responseDto.setStatusCode(200);
		responseDto.setStatusMessage("Product added in your cart");

		Mockito.when(cartService.removeCart(1, 2)).thenReturn(responseDto);
		ResponseEntity<CartResDto> result = cartController.removeCart(1, 2);

		assertEquals(HttpStatus.OK, result.getStatusCode());

	}
}
