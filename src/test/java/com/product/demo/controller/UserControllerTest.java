package com.product.demo.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.product.demo.dto.OrderResponseDto;
import com.product.demo.dto.ResponseDto;
import com.product.demo.dto.UserDto;
import com.product.demo.dto.UserReqDto;
import com.product.demo.dto.UserResDto;
import com.product.demo.exception.CredentialMissmatchException;
import com.product.demo.exception.UserExistException;
import com.product.demo.service.OrderService;
import com.product.demo.service.UserService;
import com.product.demo.utility.ErrorConstant;

/**
 * 
 * @author allam.sairam
 *
 */
//@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest
public class UserControllerTest {

	@InjectMocks
	UserController userController;

	@Mock
	UserService userService;
	@Mock
	OrderService orderService;

	OrderResponseDto orderResponseDto;
	@BeforeAll
	public void setUp() {
		orderResponseDto=new OrderResponseDto();
	}
	
	@Test
	public void userRegistration() throws UserExistException, CredentialMissmatchException {
		UserReqDto userReqDto = new UserReqDto();
		userReqDto.setPassword("madhu@123");
		userReqDto.setEmail("madhu@gmail.com");
		userReqDto.setName("Madhu");
		userReqDto.setUserType("Priority");
		userReqDto.setPhone("9591257559");

		UserResDto responseDto = new UserResDto();
		responseDto.setStatusCode(ErrorConstant.USER_REGISTERED_CODE);
		responseDto.setMessage(ErrorConstant.USER_REGISTERED + " " + userReqDto.getEmail() + " " + "Is your User Name");

		Mockito.when(userService.userRegistration(userReqDto)).thenReturn(responseDto);
		ResponseEntity<UserResDto> result = userController.userRegistration(userReqDto);

		assertEquals(HttpStatus.OK, result.getStatusCode());

	}

	@Test
	public void userLoginTest() {
		UserDto userDto = new UserDto();
		userDto.setEmail("abc@gmail.com");
		userDto.setPassword("asdf");

		ResponseDto responseDto = new ResponseDto();
		responseDto.setStatusCode(ErrorConstant.INVALID_INPUT_CODE);
		responseDto.setMessage(ErrorConstant.INVALID_INPUT);

		Mockito.when(userService.userLogin(userDto)).thenReturn(responseDto);
		ResponseEntity<ResponseDto> result = userController.userLogin(userDto);
		assertEquals(HttpStatus.OK, result.getStatusCode());
	}

	@Test
	public void getOrders() {
	
		//GIVEN
		Mockito.when(orderService.getOrders(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(Optional.of(Collections.singletonList(orderResponseDto)));
		
		//WHEN
		ResponseEntity<Optional<List<OrderResponseDto>>> result = userController.getOrders(1, 0, 5);
		//THEN
		assertNotNull(result);
	}
}
