package com.product.demo.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.product.demo.dto.OrderDto;
import com.product.demo.dto.OrderResponseDto;
import com.product.demo.dto.ResponseDto;
import com.product.demo.entity.Cart;
import com.product.demo.entity.CartProducts;
import com.product.demo.entity.OrderDetails;
import com.product.demo.entity.Products;
import com.product.demo.entity.Users;
import com.product.demo.exception.CartException;
import com.product.demo.exception.UserNotFoundException;
import com.product.demo.repository.CartProductsRepository;
import com.product.demo.repository.CartRepository;
import com.product.demo.repository.OrderDetailsRepository;
import com.product.demo.repository.ProductsRepository;
import com.product.demo.repository.UserRepository;
import com.product.demo.utility.CartStatus;
/**
 * 
 * @author allam.sairam
 *
 */
@SpringBootTest
class OrderServiceImplTest {

	@InjectMocks
	OrderServiceImpl orderServiceImpl;

	@Mock
	UserRepository userRepository;

	@Mock
	OrderDetailsRepository orderDetailsRepository;

	@Mock
	CartRepository cartRepository;
	@Mock
	CartProductsRepository cartProductsRepository;

	@Mock
	ProductsRepository productsRepository;

	OrderDto orderDto;
	Users users;
	OrderDetails orderDetails;
	CartProducts cartProducts;
	Products products;
	Cart cart;

	@BeforeEach
	public void setup() {
		orderDto = new OrderDto();
		users = new Users();
		orderDetails = new OrderDetails();
		cartProducts = new CartProducts();
		products = new Products();

		cart = new Cart();
		cart.setStatus(CartStatus.CREATED.name());

		orderDto = new OrderDto();

	}

	@Test
	void testPlaceOrder() {
		//GIVEN
		Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(users));
		Mockito.when(cartRepository.findById(Mockito.any())).thenReturn(Optional.of(cart));

		//THEN
		Optional<ResponseDto> result = orderServiceImpl.placeOrder(orderDto);

		//RETURN
		assertNotNull(result);

	}

	@Test
	void testPlaceOrderNoUser() {
		
		//GIVEN
		Mockito.when(cartRepository.findById(Mockito.any())).thenReturn(Optional.of(cart));
		try {
			orderServiceImpl.placeOrder(orderDto);

		} catch (UserNotFoundException e) {
			//RETURN
			assertEquals("invalid user", e.getMessage());
		}

	}

	@Test
	void testPlaceOrderNotProper() {
		cart.setStatus(CartStatus.ORDERED.name());
		
		//GIVEN
		Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(users));
		Mockito.when(cartRepository.findById(Mockito.any())).thenReturn(Optional.of(cart));

		//THEN
		try {
			orderServiceImpl.placeOrder(orderDto);

		} catch (CartException e) {
			
			//RETURN
			assertEquals("no valid cart existed", e.getMessage());
		}

	}

	@Test
	void testPlaceOrderNoCart() {
		cart.setStatus(CartStatus.ORDERED.name());
		
		//GIVEN
		Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(users));

		
		//THEN
		try {
			orderServiceImpl.placeOrder(orderDto);

		} catch (CartException e) {
			
			//RETURN
			assertEquals("no valid cart existed", e.getMessage());
		}

	}

	@Test
	void testGetOrders() {
		
		//GIVEN
		Mockito.when(userRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(users));
		Mockito.when(orderDetailsRepository.findByUserId(Mockito.anyInt(), Mockito.any()))
				.thenReturn(Optional.of(Collections.singletonList(orderDetails)));

		Mockito.when(cartProductsRepository.findByCartId(Mockito.anyInt()))
				.thenReturn(Optional.of(Collections.singletonList(cartProducts)));

		Mockito.when(productsRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(products));

		
		//THEN
		Optional<List<OrderResponseDto>> result = orderServiceImpl.getOrders(1, 0, 3);
		assertNotNull(result);

	}

	@Test
	void testGetOrdersNoUser() {

		//GIVEN
		Mockito.when(userRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(users));
		Mockito.when(orderDetailsRepository.findByUserId(Mockito.anyInt(), Mockito.any()))
				.thenReturn(Optional.of(Collections.singletonList(orderDetails)));

		Mockito.when(cartProductsRepository.findByCartId(Mockito.anyInt()))
				.thenReturn(Optional.of(Collections.singletonList(cartProducts)));

		Mockito.when(productsRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(products));

		
		//THEN
		try {
			orderServiceImpl.getOrders(1, 0, 3);
		} catch (UserNotFoundException e) {

			//RETURN
			assertEquals("invalid user", e.getMessage());
		}

	}

}
